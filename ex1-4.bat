@echo off

REM Experiment 1

@echo on
python train.py --batch_size=256 --run=random_wieghts --tag="Exp. 1"

python train.py --batch_size=256 --model_weights=DEFAULT --run=pretrained_weights --tag="Exp. 1"
@echo off

set folds=5
 
set batch_sizes=(5 10 15 20 25 30)
 
set learning_rates=(0.01 0.005 0.001)

REM Experiment 4 (Cross validation)
for /L %%i in (1, 1, %folds%) do (
    echo FOLD %%i
    REM Experiment 2
    for %%b in %batch_sizes% do (
        for %%l in %learning_rates% do (
            echo %%b %%l
            @echo on
            python train.py --batch_size=%%b --lr=%%l --num_epochs=1 --model_weights=DEFAULT --extra_transform=True --tag="batch_size %%b" --tag="Fold %%i" --tag="lr %%l" --tag="lr %%l" --tag="Exp. 2"
            @echo off
        )
    )

    REM Experiment 3
    @echo on
    python train.py --batch_size=256 --num_epochs=10 --model_weights=DEFAULT --extra_transform=True --run=extra_transform --tag="Fold %%i" --tag="Exp. 3"
    python train.py --batch_size=256 --num_epochs=10 --model_weights=DEFAULT --run=no_extra_transform --tag="Fold %%i" --tag="Exp. 3"
    @echo off
)