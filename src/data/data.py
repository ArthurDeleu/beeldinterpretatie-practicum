from typing import List

import torch
from torch.utils.data import DataLoader
from torchvision.transforms import v2

from .sign_dataset import SignDataset


def get_data_loaders(
    data_path: str,
    size: int,
    batch_size: int,
    val_batch_size: int,
    num_workers: int,
    num_folds: int,
    val_fold: int,
    extra_transform: bool,
    norm_mean: List[int] = [0.485, 0.456, 0.406],
    norm_std: List[int] = [0.229, 0.224, 0.225],
):
    # Create transforms
    train_tfms = v2.Compose([
        v2.ToImage(),
        v2.RandomResizedCrop(size, antialias=True),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize(mean=norm_mean, std=norm_std),
    ])

    extra_train_tfms = v2.Compose([
        v2.ToImage(),
        v2.RandomRotation(degrees=(-10, 10)),   #Borden kunnen schuin staan
        v2.RandomPerspective(distortion_scale=0.5, p=0.1), #Borden kunnen onder een andere hoek staan ten opzichte van camera
        v2.RandomResizedCrop(size, antialias=True),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize(mean=norm_mean, std=norm_std),
    ])

    val_tfms = v2.Compose([
        v2.ToImage(),
        v2.Resize(size, antialias=True),
        v2.CenterCrop(size),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize(mean=norm_mean, std=norm_std),
    ])

    # Create datasets
    train_dataset = SignDataset(data_path, subset='train', k=num_folds, val_fold=val_fold, transform=train_tfms)
    val_dataset = SignDataset(data_path, subset='val', k=num_folds, val_fold=val_fold, transform=val_tfms)
    test_dataset = SignDataset(data_path, subset='test')

    # Create data loaders
    dl_train = DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers
    )
    dl_val = DataLoader(
        val_dataset,
        batch_size=val_batch_size,
        shuffle=False,
        num_workers=num_workers,
    )

    return dl_train, dl_val
