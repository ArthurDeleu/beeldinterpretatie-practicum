from pathlib import Path
import warnings

import numpy as np
import pandas as pd
from PIL import Image
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset


# Ignore 'DataFrame.swapaxes' is deprecated warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Ignore palette images with transparency expressed in bytes warnings
warnings.simplefilter(action='ignore', category=UserWarning)


class SignDataset(Dataset):
    def __init__(self, path, subset, k=5, val_fold=0, transform=None):
        val_fold = val_fold%k
        
        dataPath = Path(path)
        self.transform = transform
        self.df = pd.DataFrame([{'label': file.parent.name, 'image': file} for file in dataPath.glob('*/*')])

        self.label_to_int =  {label : integer for integer, label in enumerate(sorted(self.df['label'].unique()))}

        
        
        df_trainval, self.test = train_test_split(self.df, train_size=0.8, random_state=42)
        folds = np.array_split(df_trainval, k)
        self.validate = folds[val_fold]
        train_folds = [fold for i, fold in enumerate(folds)
                       if i != val_fold]
        self.train = pd.concat(train_folds)


        #train, self.test = train_test_split(self.df, test_size=0.2, random_state=42)
        #self.train, self.validate = train_test_split(train, test_size=0.2, random_state=42)

        
        
        if subset == 'train':
            self.df = self.train.reset_index(drop=True)
        elif subset == 'val':
            self.df = self.validate.reset_index(drop=True)
        elif subset == 'test':
            self.df = self.test.reset_index(drop=True)
        else:
            raise ValueError(f'Unknown subset "{subset}"')        
        
    def __getitem__(self, index):
        row = self.df.iloc[index]
        img = Image.open(row['image']).convert('RGB')
       
        if self.transform is not None:
            img = self.transform(img)
        
        label = self.label_to_int[row['label']]

        return (img, label)
    
    def __len__(self):
        return len(self.df)
