import torch
from src.models import get_model
from pathlib import Path
import pandas as pd
from PIL import Image
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torchvision.transforms import v2
from typing import List
from torch import nn
from torch.utils.data import DataLoader
from sklearn.metrics import accuracy_score

best_checkpoint = "ckpts\\bcvd4du3_ep22.pth"

def loadModel(checkpoint):
    model = get_model(
        name="resnet18",
        weights="DEFAULT",
    )

    device = 'cuda'

    # Create model
    model = model.to(device)

    model.load_state_dict(torch.load(checkpoint))

    return model

class biggest_5(Dataset):
    def __init__(self, path, transform=None):        
        dataPath = Path(path)
        self.transform = transform
        df = pd.DataFrame([{'label': file.parent.name, 'image': file} for file in dataPath.glob('*/*')])

        self.label_to_int =  {label : integer for integer, label in enumerate(sorted(df['label'].unique()))}   
        
        label_counts = df['label'].value_counts()

        top_5_labels = label_counts.nlargest(5).index

        self.df = df[df['label'].isin(top_5_labels)]

        self.df.reset_index()


    def __getitem__(self, index):
        row = self.df.iloc[index]
        img = Image.open(row['image']).convert('RGB')
       
        if self.transform is not None:
            img = self.transform(img)
        
        label = self.label_to_int[row['label']]

        return (img, label)
    
    def __len__(self):
        return len(self.df)

@torch.no_grad()
def test_epoch(
    model: nn.Module,
    device: torch.device,
    dl_test: DataLoader,
):
    
    all_predictions = []
    all_labels = []

    for inputs, labels in dl_test:
        with torch.no_grad():
            outputs = model(inputs)

        _, predicted = torch.max(outputs, 1)


        all_predictions.extend(predicted.cpu().numpy())
        all_labels.extend(labels.numpy())

    accuracy = accuracy_score(all_labels, all_predictions)

    print(f'Accuracy: {accuracy * 100:.2f}%')
    return

norm_mean: List[int] = [0.485, 0.456, 0.406],
norm_std: List[int] = [0.229, 0.224, 0.225],

size = 263

test_tfm = v2.Compose([
        v2.ToImage(),
        v2.Resize(size, antialias=True),
        v2.CenterCrop(size),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize(mean=norm_mean, std=norm_std),
    ])


test_dataset = biggest_5("data/classification/", transform=test_tfm)

dl_test = DataLoader(
        test_dataset,
        batch_size=25,
        shuffle=False,
        num_workers=10,
    )

if __name__ == '__main__':
    model = loadModel(best_checkpoint)
    
    print(model)
    
    test_epoch(model=model,device='cuda', dl_test=dl_test)

